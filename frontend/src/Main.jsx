import React from 'react'
import App from './App'
import {Provider} from 'react-redux'
import {createStore} from 'redux'
const intialState={
	stock:[],
	client:[],
	purchase:[]
}

function reducer(state=intialState,action)
{	
	switch(action.type)
	{
		case "stock":return {...state,stock:action.payload}
		case "client":return {...state,client:action.payload}
		case "purchase":return {...state,purchase:action.payload}
		default:
			return state
	}
}


const store=createStore(reducer)
function Main()
{
	return <Provider store={store}><App/></Provider>
}
export default Main