import React from 'react'
import axios from 'axios'
import {useSelector,useDispatch} from 'react-redux'

const url="http://localhost/class/atiqur/15june/index.php"

function App()
{	
	let [ob1,setob1]=React.useState({name:""})
	let [ob2,setob2]=React.useState({cid:"",sid:"",volume:""})
	let state=useSelector(s=>s)
	let dispatch=useDispatch()
	let a=state.client
	let {stock,client}=state

	const view=x=>{

	}
	const profit=x=>{
		if(x.z===0)
		{	
			return ""
		}
		else
		{
			if(x.z>0){
				return "profit"
			}
			else
			{
				return "loss"
			}
		}
	}

	const color=item=>{
			if(item.z>0)
			{
				return {color:"green",fontSize:"120%"}
			}
			if( item.z<0)
			{
				return {color:"red",fontSize:"120%"}
			}
		if(item.z===0)
		{
			return {color:"grey",fontSize:"120%"}
		}
	}

	const insert=e=>{
		let {name,price}=ob1
		axios.get(`${url}?option=create-client&name=${name}`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"client",payload:d})
		})


	}
	const purchase=e=>{
		let {volume,sid,cid}=ob2
		axios.get(`${url}?option=create-purchase&cid=${cid}&sid=${sid}&volume=${volume}`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"client",payload:d})
		})
	}
	const setValues=id=>{
		setob2(a.find(x=>x.id===id))
	}

	const abc=e=>{
		axios.get(`${url}?option=client`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"client",payload:d})
		})
		axios.get(`${url}?option=stock`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"stock",payload:d})
		})

		axios.get(`${url}?option=purchase`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"purchase",payload:d})
		})

		
		
	}

	React.useEffect(abc,[])

	return <div>
		<h1>clients</h1>	
		<datalist id="clist">
			{client.map(x=>
				<option value={x.id}>{x.name}</option>
			)}
		</datalist>	
		<datalist id="slist">
			{stock.map(x=>
				<option value={x.id}>{x.name}/{x.price}</option>
			)}
		</datalist>	
		
		<div className="flex">
			<div className="item">
				<h5>create client</h5>
				<div className="form-flex">
					<button>@</button>
					<input value={ob1.name} onChange={e=>setob1({name:e.target.value})} placeholder="name" />
				</div>
				<button className="btn" onClick={insert}>add</button>
			</div>

			<div className="item">
				<h5>update</h5>
				<div className="form-flex">
					<input onChange={e=>setob2({...ob2,cid:e.target.value})} value={ob2.cid} list="clist" placeholder="choose client" />
				</div>
				<div className="form-flex">
					<input onChange={e=>setob2({...ob2,sid:e.target.value})} value={ob2.sid} list="slist" placeholder="choose stock" />
				</div>
				<div className="form-flex">
					<input onChange={e=>setob2({...ob2,volume:e.target.value})} value={ob2.volume} placeholder="volume" />
				</div>
				<button className="btn" onClick={purchase}>save</button>
			</div>
			<div className="item">
				<h5>all client</h5>
				<table >
					<thead>
						<tr>
							<th>client</th>
							<th>balance</th>
							<th>gain/loss</th>
							<th>actions</th>
						</tr>
					</thead>
					<tbody>
						{a.map(x=>
							<tr key={x.id}>
								<td>{x.name}</td>
								
								<td>
									<strong>
										<i>{x.amount}</i>
									</strong>
								</td>
								<td style={color(x)}>
									{x.z}
									{profit(x)}
								</td>
								<td>
									<button>...</button>
									<ul>
										<li onClick={e=>view(x)}>view</li>
									</ul>
								</td>
							</tr>
						)}
					</tbody>
				</table>
			</div>
		</div>
	</div>
}
export default App