import React from 'react'
import Stock from './Stock'
import Client from './Client'
import Purchase from './Purchase'
function App()
{	
	return <div>
		<Stock />
		<Client />
		<Purchase />
	</div>
}
export default App