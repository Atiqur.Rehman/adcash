import React from 'react'
import axios from 'axios'
import {useSelector,useDispatch} from 'react-redux'
const url="http://localhost/class/atiqur/15june/index.php"

function App()
{	

	let [ob1,setob1]=React.useState({name:"",price:""})
	let [ob2,setob2]=React.useState({id:0,name:"",price:""})
	let state=useSelector(s=>s)
	let dispatch=useDispatch()
	
	let a=state.stock

	const edit=x=>{
		setob2(x)
	}
	const del=id=>{
		axios.get(`${url}?option=delete-stock&id=${id}`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"stock",payload:d})
		})
		
	}
	const insert=e=>{
		let {name,price}=ob1
		axios.get(`${url}?option=create-stock&name=${name}&price=${price}`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"stock",payload:d})
		})


	}
	const update=e=>{
		let {id,name,price}=ob2
		axios.get(`${url}?option=update-stock&id=${id}&price=${price}`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"stock",payload:d})
		})
	}
	const setValues=id=>{
		setob2(a.find(x=>x.id===id))
	}

	const abc=e=>{
		axios.get(`${url}?option=stock`)
		.then(res=>res.data)
		.then(d=>{
			console.log(d)
			dispatch({type:"stock",payload:d})
		})
	}

	React.useEffect(abc,[])

	return <div>
		<h1>Stocks</h1>	
		<datalist id="mylist">
			{a.map(x=>
				<option value={x.id}>{x.name}</option>
			)}
		</datalist>
		<div className="flex">
			<div className="item">
				<h5>create stock</h5>
				<div className="form-flex">
					<input value={ob1.name} onChange={e=>setob1({...ob1,name:e.target.value})} placeholder="company" />
				</div>
				<div className="form-flex">
					<button>@</button>
					<input value={ob1.price} onChange={e=>setob1({...ob1,price:e.target.value})} placeholder="price" />
				</div>
				<button className="btn" onClick={insert}>add</button>
			</div>

			<div className="item">
				{	
					ob2.id>0
					&&
				<>
					<h5>update</h5>
					<div className="form-flex">
						<input list="mylist" value={ob2.name} onChange={e=>setValues(e.target.value)} placeholder="choose stock" />
					</div>
					<div className="form-flex">
						<button>@</button>
						<input value={ob2.price} onChange={e=>setob2({...ob2,price:e.target.value})} placeholder="price" />
					</div>
					<button className="btn" onClick={update}>save</button>
				</>
				}
			</div>
			<div className="item">
				<h5>all stock</h5>
				<table >
					<thead>
						<tr>
							<th>company</th>
							<th>price</th>
							<th>date</th>
							<th>actions</th>
						</tr>
					</thead>
					<tbody>
						{a.map(x=>
							<tr key={x.id}>
								<td>{x.name}</td>
								<td>{x.price}</td>
								<td>{x.added_on}</td>
								<td>
									<button>...</button>
									<ul>
										<li onClick={e=>edit(x)}>udpate</li>
										<li onClick={e=>del(x.id)}>delete</li>
									</ul>
								</td>
							</tr>
						)}
					</tbody>
				</table>
			</div>
		</div>
	</div>
}
export default App